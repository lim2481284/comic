<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>iComic</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Nunito" />
    <link rel="shortcut icon" href="/img/icon/icomic.ico"/>
    <link href="/css/bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/app.animation.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/app.main.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/app.responsive.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>

    <!-- Script -->
    <script type="text/javascript" src="/js/jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/jquery.bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/jquery.nicescroll.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/app.main.min.js{{ config('app.link_version') }}"></script>

    @yield('head')
</head>

<body>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <svg class="circular" viewBox="25 25 50 50">
    			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
             </svg>
            <label class='small'> {{ trans('lang.loading') }} </label>
        </div>
    </div>

    <!--Login section -->
    <div class="login-form-section">
        <div class='login-form-outside'></div>
        <div class="login-form-panel">
            <button class='close-login-form-btn'>
                <i class="fa fa-close"></i>
            </button>
            <form action ="" method = "post" class="register-form" onsubmit="return check()">
                <label>{{trans('lang.username')}}</label>
                {{ Form::input('text', 'username', null , ['class' => 'form-control ', 'placeholder'=>trans('lang.username'), 'autofocus'=>'autofocus','required'=>'required']) }}
                <label>{{trans('lang.password')}}</label>
                {{ Form::input('password', 'password', null , ['class' => 'form-control ', 'placeholder'=>trans('lang.password'), 'required'=>'required']) }}
                <label>{{trans('lang.confirm_password')}}</label>
                {{ Form::input('password', 'confirm_password', null , ['class' => 'form-control ', 'placeholder'=>trans('lang.confirm_password'), 'required'=>'required']) }}
                <label>{{trans('lang.email')}}</label>
                {{ Form::input('email', 'email', null , ['class' => 'form-control ', 'placeholder'=>trans('lang.email'), 'required'=>'required']) }}
                <button class='btn' id='register' name="register">
                    {{trans('lang.create_account')}}
                </button>
                <p class="login-message">
                    {{trans('lang.registered')}}
                    <a href="#">{{trans('lang.sign_in')}}</a>
                </p>
            </form>
            <form action ="" method = "post"  class="login-form">
                <img src ='/img/logo/icomic-logo-full.png' class='login-logo'>
                <label>{{trans('lang.username')}}</label>
                {{ Form::input('text', 'username', null , ['class' => 'form-control ', 'placeholder'=>trans('lang.username'), 'autofocus'=>'autofocus','required'=>'required']) }}
                <label>{{trans('lang.password')}}</label>
                {{ Form::input('password', 'password', null , ['class' => 'form-control ', 'placeholder'=>trans('lang.password'), 'required'=>'required']) }}
                <button class='btn' type='submit' name="login">
                    {{trans('lang.login')}}
                </button>
                <p class="login-message">
                    {{trans('lang.no_account')}}
                    <a href="#">{{trans('lang.create_account')}}</a>
                </p>
            </form>
        </div>
    </div>

    <!-- Top Menu Section -->
    <section class="top-menu">
        <header id="top-menu-header">
            <div class="header-content">
                <a class="logo" href="/"><img src='/img/logo/icomic-logo-full.png'/></a>
                <nav class="navigation">
                    <ul class="primary-nav">
                        <li>
                            {{ Form::select('localization', LaravelLocalization::getSupportedLocalesNative(), LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() , null, [], true)  , ['class' => 'form-control localization col-centered']) }}
                        </li>
                        <li>
                            <button type='button' class='btn login-btn'>
                                {{ trans('lang.login') }}
                            </button>
                        </li>
                    </ul>
                </nav>
                <a href="#" class="nav-toggle">
                    {{ trans('lang.menu') }}
                    <span></span>
                </a>
            </div>
        </header>
    </section>

    <!-- Body Section -->
    <section class="body-content">
        @yield('content')
    </section>

    <!-- Footer Section -->
    <footer id="contact" class="footer">
      <div class="container-fluid">
          <div class='row'>
            <div class="col-sm-5">
              <h4>{{ trans('lang.office') }}</h4>
              <p> Collins Street West Victoria 8007 Australia.</p>
            </div>
            <div class="col-sm-4">
              <h4>{{ trans('lang.contact') }}</h4>
              <p> {{ trans('lang.call') }}: 612.269.8419 <br>
                {{ trans('lang.email') }} : <a href="mailto:hello@agency.com"> hello@agency.com </a></p>
            </div>
            <div class="col-sm-3">
              <h4>{{ trans('lang.social') }}</h4>
              <ul class="footer-share">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
        </div>
        <div class='row'>
            <div class="col-sm-12 footer-copyright">
                <p>
                    © 2018 {{ trans('lang.copyright_reserved') }}.<br>
                    <a href="http://www.designstub.com/">
                        {{ trans('lang.copyright_powered') }}
                    </a>
                </p>
            </div>
        </div>
      </div>
    </footer>
</body>
</html>
