<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Meta and title -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>iComic - Dashboard</title>

    <!-- Icon and font  -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Nunito" />
    <link rel="shortcut icon" href="/img/icon/icomic.ico"/>

    <!-- Styles -->
    <link href="/css/jquery.bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/dashboard.main.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard.responsive.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>

    <!-- Script -->
    <script type="text/javascript" src="/js/jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/jquery.bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/dashboard.sidebar-menu.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/dashboard.main.min.js{{ config('app.link_version') }}"></script>

    @yield('head')
</head>

<body  class="fix-header fix-sidebar">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <div id="main-wrapper">
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <b><img src="images/logo.png" alt="homepage" class="dark-logo" /></b>
                        <span><img src="images/logo-text.png" alt="homepage" class="dark-logo" /></span>
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- Search -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search here"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>

                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="images/users/5.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="#"><i class="ti-user"></i> Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li><a href="#"><i class="ti-settings"></i> Setting</a></li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="left-sidebar">
			<nav class="sidebar-nav">
				<ul id="sidebarnav">
					<li class="nav-label">Home</li>
					 <li><a href="index.html">Ecommerce </a></li>
					 <li><a href="index1.html">Analytics </a></li>
				</ul>
			</nav>
        </div>
        <!-- Body Section -->
        <div class="page-wrapper">
            @yield('content')
        </div>
    </div>
</body>
</html>
