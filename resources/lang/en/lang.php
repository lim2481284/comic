<?php
   return [
      'home'                => 'Home',
      'login'               => 'Login',
      'landing_title'       => 'TRAVEL. EXPLORE. DISCOVER.',
      'landing_description' => 'Find activities, attractions , tours & more !',
      'search_placeholder'  => 'Search country, attractions, tours...',
      'recommendation'      => 'Recommendation',
      'office'              => 'Office Location',
      'contact'             => 'Contact',
      'social'              => 'Social',
      'copyright_reserved'  => 'All Rights Reserved',
      'copyright_powered'   => 'Designed & Powered By Pan FarEast',
      'call'                => 'Call',
      'email'               => 'Email',
      'menu'                => 'Menu',
      'loading'             => 'Loading',
      'username'            => 'Username',
      'password'            => 'Password',
      'confirm_password'    => 'Confirm Password',
      'registered'          => 'Already register ?',
      'sign_in'             => 'Sign In',
      'no_account'          => 'No account ?',
      'create_account'      => 'Create an account'

   ];
?>
