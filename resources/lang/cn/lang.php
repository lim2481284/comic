<?php
   return [
      'home'                => '首页',
      'login'               => '登录',
      'landing_title'       => '<b>旅行. 探索. 发现.</b>',
      'landing_description' => '查找活动，景点，旅游和更多!',
      'search_placeholder'  => '搜索国家，景点，旅游...',
      'recommendation'      => '推荐',
      'office'              => '办公室位置',
      'contact'             => '联系',
      'social'              => '社交',
      'copyright_reserved'  => '版权',
      'copyright_powered'   => '由Pan FarEast设计和支持',
      'call'                => '电话',
      'email'               => '电子邮件',
      'menu'                => '菜单',
      'loading'             => '加载中',
      'username'            => '用户名字',
      'password'            => '密码',
      'confirm_password'    => '确认密码',
      'registered'          => '已经有户口 ?',
      'sign_in'             => '登录',
      'no_account'          => '没有户口 ?',
      'create_account'      => '创建户口'
   ];
?>
