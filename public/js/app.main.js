/* ------------------------------------------------------------------------------
	 Sharing function section
-------------------------------------------------------------------------------*/

function hideLoader(){
	$('.page-loader').fadeOut('slow');
}

function showLoader(){
    $('.page-loader').fadeIn('slow');
}

/* ------------------------------------------------------------------------------
	 General script
-------------------------------------------------------------------------------*/

$(document).ready(function() {
	
	//Search animation : if not input
	$('.search-btn').click(function(){
		if(!$('.search-input').val())
		{
			element = document.getElementById("search-input-section");;
			element.classList.remove("shake");
			void element.offsetWidth;
			element.classList.add("shake");
		}
	})

	//Localization function
	$(document).on('change','.localization',function(){
		location.href=$(this).val();
	})

	//Scrollbar design
	$("body").niceScroll({styler:"fb",cursorcolor:"#81DBF8", cursorwidth: '8', cursorborderradius: '10px', background: '#FFFFFF', spacebarenabled:false, cursorborder: '0',  zindex: '1000',  autohidemode: false});

	// Mobile Navigation
	var nav = $('.navigation');
	$('.nav-toggle').on('click', function() {
		$(this).toggleClass('close-nav');
		nav.toggleClass('open');
		return false;
	});
	nav.find('a').on('click', function() {
		$('.nav-toggle').toggleClass('close-nav');
		nav.toggleClass('open');
	});

	//Login popup function
	$(document).on('click','.login-btn',function(){
		$('.login-form-section').fadeIn('slow');
	});
	$(document).on('click','.login-message a',function(){
		$('.login-form-panel form').animate({
			height: "toggle",
			opacity: "toggle"
		}, "slow");
	});
	$(document).on('click','.close-login-form-btn, .login-form-outside',function(){
		$('.login-form-section').fadeOut('fast');
	})

	//Hide loader
	hideLoader();

});
