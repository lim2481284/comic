<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookChapterTranslateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_chapter_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('image_id');
            $table->foreign('image_id')->references('id')->on('book_chapter_images')->onDelete('set null');
            $table->longText('translate_text')->nullable();
            $table->integer('x1_axis');
            $table->integer('x2_axis');
            $table->integer('y1_axis');
            $table->integer('y2_axis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_chapter_translate');
    }
}
