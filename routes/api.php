<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::get('/test', function () {
    return dd('test');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
