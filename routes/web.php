<?php

//Localization route grouping
Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],function()
{
    //Set language translator
    app()->setLocale(LaravelLocalization::getCurrentLocale());

    //Homepage routing
    Route::get('/', function () {
        return view('app.index');
    });

    //Homepage routing
    Route::get('/dashboard', function () {
        return view('dashboard.index');
    });


});
